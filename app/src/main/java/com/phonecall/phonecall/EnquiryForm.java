package com.phonecall.phonecall;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EnquiryForm extends AppCompatActivity implements AdapterView.OnItemSelectedListener,View.OnClickListener {
    private TextView mobNo;
    private EditText name;
    private EditText enquiryType;
    private ImageView imageViewBack;
    private Button buttonSubmit;


    private EditText editTextDetails;
    private EditText editTextEmailId;
    private DatabaseReference database;

    // private DatabaseReference database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry_form);


        mobNo = (TextView) findViewById(R.id.editTextMobileNo);
        editTextDetails = (EditText) findViewById(R.id.editTextDetails);
        editTextEmailId = (EditText) findViewById(R.id.editTextEmailId);
        name = (EditText) findViewById(R.id.editTextName);
        imageViewBack = (ImageView) findViewById(R.id.imageViewBack);

        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);

        database = FirebaseDatabase.getInstance().getReference();

        buttonSubmit.setOnClickListener(new View.OnClickListener() {

    @Override
    public void onClick(View view) {

    }

    public void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }


  /* // Spinner element
    Spinner spinner = (Spinner) findViewById(R.id.EnquiryType);

    // Spinner click listener
    //    spinner.setItemClickListener(this);

    // Spinner Drop down elements
    ArrayList<String> categories= new ArrayList<String>();
        categories.add("Job Enquiry");
        categories.add("Software Developer Enquiry");
        categories.add("Business Enquiry");
        categories.add("Education Enquiry");
        categories.add("Personal Loan Enquiry");


    // Creating adapter for spinner
    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);


    // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
}
*/
    @Override
    public void onItemSelected(AdapterView parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

    }

    public void onNothingSelected(AdapterView arg0) {
        // TODO Auto-generated method stub

    }


}
